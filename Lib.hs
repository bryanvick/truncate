{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternGuards     #-}

--------------------------------------------------------------------------------
module Lib where

--------------------------------------------------------------------------------
import           Prelude ( Int, Float, String, Maybe(..)
                         , (*), (==), (!!), ($)
                         , truncate, otherwise, fst, reads, length, show
                         )
import qualified Data.Text as T
import           Text.Digestive.Types ( Result(Error, Success) )
import           Debug.Trace ( trace )


--------------------------------------------------------------------------------
type Err = T.Text


--------------------------------------------------------------------------------
-- | Should parse "0.12" to Success 12.
validateVal :: T.Text -> Result Err (Maybe Int)
validateVal t
  | isEmpty                = Success Nothing
  | (Success f) <- asFloat = Success $ Just $ ((truncate (f * 100))::Int)
  | otherwise              = Error $ T.append "Not a number: " t
  where isEmpty = T.null t
        asFloat = validateFloat t


--------------------------------------------------------------------------------
-- |The same as validateVal, but use trace to force/show "f * 100".
unsafeValidateVal :: T.Text -> Result Err (Maybe Int)
unsafeValidateVal t
  | isEmpty                = Success Nothing
  | (Success f) <- asFloat = let asInt = f * 100
                                 ans   = Success $ Just $ ((truncate asInt)::Int)
                             in trace (show asInt) ans
  | otherwise              = Error $ T.append "Not a number: " t
  where isEmpty = T.null t
        asFloat = validateFloat t


--------------------------------------------------------------------------------
-- |Parse "0.12" into Success 0.12.  This seems to work fine.
validateFloat :: T.Text -> Result Err Float
validateFloat t = if len == 1 then Success $ fst (parse !! 0) else err
  where s     = T.unpack t
        parse = (reads s)::[(Float, String)]
        len   = length parse
        err   = Error $ T.append "Not a number: " t
