{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternGuards     #-}


-- |A web app showing the unexpected behavior.  We use the digestive-functor
-- library to parse a web form into a Thing.  The val field of the Thing should
-- be 12 when the user enteres "0.12" into the web form, but it is 11.
--
-- See Lib.hs for the parsing functions that are causing problems.
module Main (main) where

--------------------------------------------------------------------------------
import           Prelude ( IO, Show, Int, Float, String, Maybe(..)
                         , ($), (++), (==), (.), (*), (!!)
                         , putStrLn, show, fst, snd, return, fmap, filter
                         , truncate, otherwise, reads, length
                         )
import           Control.Monad ( Monad )
import           Control.Monad.IO.Class ( liftIO )
import           Control.Applicative  ( (<$>), (<*>) )
import           Data.Maybe ( fromJust )
import qualified Data.Text as T
import           Data.Text.Encoding ( decodeUtf8 )
import qualified Text.Blaze.Html5 as H
import           Text.Blaze.Html.Renderer.Utf8 ( renderHtmlBuilder )
import           Text.Digestive.Form  ( Form, (.:), text, validate )
import           Text.Digestive.View  ( View, getForm, postForm )
import           Text.Digestive.Types ( Env, FormInput(TextInput, FileInput)
                                      , fromPath
                                      )
import qualified Text.Digestive.Blaze.Html5 as HD
import           Network.HTTP.Types.Status ( status200 )
import           Network.HTTP.Types.Method ( methodGet )
import           Network.Wai ( Application, Request, Response(ResponseBuilder)
                             , requestMethod
                             )
import           Network.Wai.Parse ( parseRequestBody, tempFileBackEnd, Param
                                   , File, FileInfo(fileName)
                                   )
import           Network.Wai.Handler.Warp ( run )

--------------------------------------------------------------------------------
import qualified Lib as Lib


--------------------------------------------------------------------------------
main :: IO ()
main = do putStrLn $ "running at port " ++ (show port)
          run port app


--------------------------------------------------------------------------------
port :: Int
port = 8005


--------------------------------------------------------------------------------
app :: Application
app req = if requestMethod req == methodGet
          then getEditForm req
          else postEdit req




--------------------------------------------------------------------------------
data Thing = Thing {name :: T.Text, val :: Maybe Int} deriving (Show)
type Err   = T.Text


--------------------------------------------------------------------------------
-- |A digestive-functor form that turns a web form into a Thing.  See
-- Lib.validateVal for the code that turns "0.12" into 12::Int that is causing
-- the problems.
form :: (Monad m) => Form Err m Thing
form = Thing
    <$> "name" .: text (Just "name")
    <*> "val"  .: validate Lib.validateVal (text $ Just "0.12")


--------------------------------------------------------------------------------
editForm :: View H.Html -> H.Html
editForm view = do
  HD.form view postUrl $ do
    H.fieldset $ do
      labledTextInput view "name" "Name"
      labledTextInput view "val" "Val"
      H.div $ HD.inputSubmit "Save"
  where postUrl = "/"


--------------------------------------------------------------------------------
labledTextInput :: View H.Html -> T.Text -> H.Html -> H.Html
labledTextInput view name' lbl = do HD.label name' view lbl
                                    HD.errorList name' view
                                    HD.inputText name' view


--------------------------------------------------------------------------------
getEditForm :: Application
getEditForm _ = do
  view <- getForm "" form
  let html = editForm (fmap H.toHtml view)
      content = renderHtmlBuilder html
  return $ ResponseBuilder status200 headers content
    where headers = [("Content-type", "text/html")]


--------------------------------------------------------------------------------
postEdit :: Application
postEdit req = do
  env <- fmap waiEnv $ parseRequestBody tempFileBackEnd req
  (_, maybeThing) <- postForm "" form env
  let thing = fromJust maybeThing
  liftIO $ putStrLn (show thing)
  return $ ResponseBuilder status200 [] $ renderHtmlBuilder (H.toHtml (T.pack $ show thing))


--------------------------------------------------------------------------------
-- |Get a digestive-functors Env wai-extra parsed body.
waiEnv :: (Monad m) => ([Param], [File y]) -> Env m
waiEnv parsedBody =  let textInputs = fmap paramToTextInput $ params parsedBody
                         fileInputs = fmap fileToFileInput $ files parsedBody
                     in find (textInputs ++ fileInputs) . fromPath
  where
    params      = fst
    files       = snd
    match fname = (== fname) . fst
    paramToTextInput (name', value) = ( decodeUtf8 name'
                                      , TextInput $ decodeUtf8 value
                                      )
    fileToFileInput (name', fileInfo) = ( decodeUtf8 name'
                                        , FileInput 
                                          $ T.unpack
                                          $ decodeUtf8 (fileName fileInfo)
                                        )
    find lst fname = return $ fmap snd $ filter (match fname) lst
